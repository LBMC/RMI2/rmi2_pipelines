version = "2.1.0"
container = "lbmc/hisat2:${version}"

process paired_end {
  tag "$pair_id"
  label "big_mem_multi_cpus"
  publishDir "${params.output}/hisat2", mode: 'copy'

  input:
  tuple val(pair_id), file(fastq)
  file(index)
  val(output)

  output:
  tuple val(pair_id), file("*.bam"), emit: BAM
  file("*_report.txt"), emit: LOGS
  tule val(pair_id), file("*.fastq.gz"), emit : FASTQ

  script:
  index_id = index[0]
  for (index_file in index) {
    if (index_file =~ /.*\.1\.ht2/ && !(index_file =~ /.*\.rev\.1\.ht2/)) {
        index_id = ( index_file =~ /(.*)\.1\.ht2/)[0][1]
    }
  }
"""
hisat2 -x ${index_id} \
       -p ${task.cpus} \
       -1 ${fastq[0]} \
       -2 ${fastq[1]} \
       --un-conc-gz ${pair_id}_notaligned_R%.fastq.gz \
       --rna-strandness 'FR' \
       --dta \
       --no-softclip\
       --trim3 1\
       --trim5 1\
       2> ${pair_id}_report.txt \
| samtools view -bS -F 4 - \
| samtools sort -@ ${task.cpus} -o ${pair_id}.bam \
&& samtools index ${pair_id}.bam

if grep -q "ERR" ${pair_id}.txt; then
  exit 1
fi

"""
}
