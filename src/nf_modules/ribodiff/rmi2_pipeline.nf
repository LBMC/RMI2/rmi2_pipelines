version = "0.2.2--py_2"
container_url = "quay.io/biocontainers/ribodiff:${version}"

process ribodiff {
  container = "${container_url}"
  label "big_mem_mono_cpus"
  publishDir "${output}", mode: 'copy', pattern: "*"

  input:
    path exp_outline
    path cnt_table
    val output

  output:
    path "*", emit: ribodiff_out

  script:
"""
TE.py -e ${exp_outline} -c ${cnt_table} -o result.txt -p 1
"""
}
