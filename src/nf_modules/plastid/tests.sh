./nextflow src/nf_modules/plastid/plastid_generate.nf \
  -c src/nf_modules/plastid/plastid.config \
  -profile docker \
  --annotation_files "data/tiny_dataset/annot/tiny.gtf" \
  -resume
