params.outputdir = "results/plastid/psite"
params.roi = "data/*roi.txt"
params.bam = "data/*.bam"
params.min_length = 29
params.max_length = 35

log.info "output dir : ${params.outputdir}"
log.info "bam files : ${params.bam}"
log.info "ROI files : ${params.roi}"
log.info "min length : ${params.min_length}"
log.info "max length : ${params.max_length}"

Channel
  .fromPath( params.roi )
  .ifEmpty { error "Cannot find any files matching: ${params.roi}" }
  .set { ROI_FILES }

Channel
  .fromFilePairs( params.bam )
  .ifEmpty { error "Cannot find any files matching: ${params.bam}" }
  .set { BAM_FILES }

process plastid_psite {
  tag "$file_id"
  publishDir "${params.outputdir}", mode: 'copy'

  input:
  set file_id, file(bam) from BAM_FILES
  file roi from ROI_FILES.collect()

  output:
  file "*" into PSITE_FILES

  script:
"""
psite ${roi} \
      ${file_id}\
      --min_length ${params.min_length}\
      --max_length ${params.max_length}\
      --require_upstream \
      --count_files ${bam[0]}
"""
}
