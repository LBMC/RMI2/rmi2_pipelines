params.outputdir = "results/plastid/metagene"
params.roi = "data/*roi.txt"
params.bam = "data/*.bam"
params.offset = 12
params.min_count = 50
params.landmark = "cds_start"


log.info "output dir : ${params.outputdir}"
log.info "bam files : ${params.bam}"
log.info "ROI files : ${params.roi}"
log.info "offset : ${params.offset}"
log.info "min count : ${params.min_count}"

Channel
  .fromFilePairs( params.bam )
  .ifEmpty { error "Cannot find any files matching: ${params.bam}" }
  .set { BAM_FILES }

Channel
  .fromPath( params.roi )
  .ifEmpty { error "Cannot find any files matching: ${params.roi}" }
  .set { ROI_FILES }

process plastid_metagene {
  tag "${file_id}"
  publishDir "${params.outputdir}", mode: 'copy'

  input:
  set file_id, file(bam) from BAM_FILES
  file roi from ROI_FILES.collect()

  output:
  file "*metagene_profile.txt" into METAGENE_FILES

  script:
"""
metagene count ${roi} ${file_id} \
               --count_files ${bam[0]}\
               --fiveprime \
               --offset ${params.offset} \
               --normalize_over 30 200 \
               --min_counts ${params.min_count} \
               --cmap Blues \
               --title "${file_id}"

"""
}

process plastid_chart {
  tag "${file_id}"
  publishDir "${params.outputdir}", mode: 'copy'

  input:
  file metagene_profile from METAGENE_FILES.collect()

  output:
  file "*" into CHART_files

  script:
"""
metagene chart --landmark "${params.landmark}" \
               --title "metagene plot"\
               Metachart\
               *metagene_profile.txt


"""
}
