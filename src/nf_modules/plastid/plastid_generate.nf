params.outputBaseName = "plastid_annot"
params.landmark = "cds_start"
params.annotation_files = "data/*.gtf"
params.downstream = 120
params.outputdir = "results/plastid/generate"

log.info "gff files : ${params.annotation_files}"

Channel
.fromPath( params.annotation_files )
.ifEmpty { error "Cannot find any gff files matching: ${params.annotation_files}" }
.set { ANNOT_FILES }

process plastid_generate {
  tag "plastid_generate"
  publishDir "${params.outputdir}", mode: 'copy'

  input:
  file annot from ANNOT_FILES

  output:
  file "*" into ANNOTED_FILES

  script:
"""
metagene generate ${params.outputBaseName} \
       --landmark ${params.landmark} \
       --annotation_files ./${annot} \
       --downstream ${params.downstream}

"""
}
