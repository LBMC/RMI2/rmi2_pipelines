params.bam = "$baseDir/data/bam/*.bam"

log.info "fastq files : ${params.bam}"

Channel
  .fromPath( params.bam )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.bam}" }
  .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
  .set { bam_files }

///////////////////////////////////////////////////////////////////////////////
// TRANSCRIPTOME BUILDING

process stringtie {
  tag "$file_id"
  publishDir "results/stringtie/${file_id}", mode: 'copy'

 input:
 set file_id, file(bam) from bam_files

 output:
 file "*.gtf" into gtf_transcriptome

 script:
 """
stringtie -L \
          -o ${file_id}.gtf \
          -f 0.001 \
          -j 0.5 ${bam}
 """
}

// END STRNGTIE
