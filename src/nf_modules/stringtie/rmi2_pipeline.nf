version = "2.1.5--h978d192_1"
container_url = "quay.io/biocontainers/stringtie:${version}"


///////////////////////////////////////////////////////////////////////////////
// TRANSCRIPTOME BUILDING

process assembly_from_longreads {
  container = "${container_url}"
  tag "$file_id"
  label "big_mem_multi_cpus"
  publishDir "${output}/${file_id}", mode: 'copy'

 input:
 tuple val(file_id), file(bam)
 val output

 output:
 path("*.gtf"), emit: GTF

 script:
 """
stringtie -o ${file_id}.gtf \
          -f 0.01 \
          -p ${task.cpus}\
          -j 0.5 \
          ${bam}
 """
}

process assembly_from_RNAseq {
  container = "${container_url}"
  label "big_mem_multi_cpus"
  tag "$file_id"
//  publishDir "results/stringtie/${file_id}", mode: 'copy'

 input:
 tuple val(file_id), file(bam)
 file(gtf)

 output:
 path("*.gtf"), emit: GTF

 script:
 """
stringtie -p ${task.cpus}\
          -G ${gtf} \
          -o ${file_id}.gtf \
          ${bam}
 """
}

process merge_transcriptomes {
  container = "${container_url}"
  label "big_mem_multi_cpus"
  tag "merging transcriptome"

  input:
    file(transcriptome)
    file(gtf)

  output:
    path("transcriptome_merged.gtf"), emit: MERGED_GTF

  script:
"""
stringtie --merge -G ${gtf} -o transcriptome_merged.gtf ${transcriptome}
"""
}

process abundance {
  container = "${container_url}"
  tag "${file_id}"
  label "big_mem_multi_cpus"
  publishDir "${output}/stringtie/${file_id}", mode: 'copy'

  input:
    tuple val(file_id), file(bam)
    file(gtf)
    val(output)

  output:
    tuple val(file_id), path("*"), emit: ABUNDANCE

  script:
"""
stringtie -p ${task.cpus} -e -B -G ${gtf} -o ${file_id}.gtf ${bam}
"""
}
