params.bam = "$baseDir/data/bam/*.bam"
params.dedup_options = ""

log.info "bam files : ${params.bam}"
log.info "aditionnal option for umi_tools dedup : ${params.options}"

Channel
  .fromPath( params.bam )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.bam}" }
  .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
  .set { bam_files }

process sort_bam {
  tag "$file_id"
  cpus 4

  input:
    set file_id, file(bam) from bam_files

  output:
    set file_id, "*_sorted.bam{,.bai}" into sorted_bam_files

  script:
"""
# sort bam
samtools sort -@ ${task.cpus} -o ${file_id}_sorted.bam ${bam}
samtools index ${file_id}_sorted.bam
"""
}

process dedup {
  tag "$file_id"
  publishDir "results/dedup/", mode: 'copy'

  input:
  set file_id, file(bam) from sorted_bam_files

  output:
  file "*dedup.bam" into dedup_bam
  file "*.txt" into dedup_report

  script:
"""
umi_tools dedup -I ${bam[0]} \
                ${params.options} \
                -S ${file_id}_dedup.bam > report.txt
"""
}
