./nextflow src/nf_modules/umi_tools/dedup.nf \
  -c src/nf_modules/umi_tools/dedup.config \
  -profile docker \
  --bam "data/tiny_dataset/map/tiny_v2.bam" \
  -resume

if [ -x "$(command -v singularity)" ]; then
./nextflow src/nf_modules/umi_tools/dedup.nf \
  -c src/nf_modules/umi_tools/dedup.config \
  -profile singularity \
  --bam "data/tiny_dataset/map/tiny_v2.bam" \
  -resume
fi
