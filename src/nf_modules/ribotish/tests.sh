./nextflow src/nf_modules/ribotish/ribotish_quality.nf \
  -c src/nf_modules/ribotish/ribotish.config \
  -profile docker \
  --bam "data/tiny_dataset/map/tiny_v2.bam" \
  --gff "data/tiny_dataset/annot/tiny.gtf" \
  -resume
