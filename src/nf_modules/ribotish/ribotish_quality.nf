params.bam = "data/*{.bam,.bam.bai}"
params.gff = "data/*.gtf"
params.length = "25,35"
params.th = "0.5"
params.output = "results/quality"

log.info "bam files : ${params.bam}"
log.info "gff file : ${params.gff}"
log.info "footprint length : ${params.length}"
log.info "threashold : ${params.th}"
log.info "output dir : ${params.output}"

Channel
  .fromFilePairs( params.bam )
  .ifEmpty { error "Cannot find any bam files matching: ${params.bam}" }
  .set { bam_files }
Channel
  .fromPath( params.gff )
  .ifEmpty { error "Cannot find any gff files matching: ${params.gff}" }
  .set { gff_files }

process ribotish_quality {
  tag "$file_id"
  publishDir "${params.output}", mode: 'copy'

  input:
  set file_id, file(bam) from bam_files
  file gff from gff_files.collect()

  output:
  set file_id, "*" into quality

  script:
  tis_parameters = ""
  if (bam =~ /Harr/) {
    tis_parameters = "-t"
  }
"""
ribotish quality -b ${bam[0]} \
                 -g ${gff} \
                 -p ${task.cpus} \
                 -l ${params.length} \
                 --th ${params.th} \
                 ${tis_parameters}
"""
}
