params.bam = "data/*{Harr,CHX}*.bam"
params.bai = "data/*{Harr,CHX}*.bam.bai"
params.gff = "data/*.gtf"
params.genome = "data/*.fa"
params.quality = "results/quality/*"

log.info "bam files : ${params.bam}"

Channel
.fromFilePairs( params.bam )
.ifEmpty { error "Cannot find any bam files matching: ${params.bam}" }
.set { bam_files }
Channel
.fromFilePairs( params.bai )
.ifEmpty { error "Cannot find any bai files matching: ${params.bai}" }
.set { bai_files }
Channel
  .fromPath( params.gff )
  .ifEmpty { error "Cannot find any gff files matching: ${params.gff}" }
  .set { gff_files }
Channel
  .fromPath( params.genome )
  .ifEmpty { error "Cannot find any fasta files matching: ${params.genome}" }
  .set { genome_files }
Channel
  .fromPath( params.files )
  .ifEmpty { error "Cannot find any files matching: ${params.quality}" }
  .set { quality_files }

process ribotish_pred {
  tag "$file_id"
  publishDir "results/pred/", mode: 'copy'

  input:
  set file_id, file(bam) from bam_files
  set bai_id, file(bai) from bai_files
  file gff from gff_files.collect()
  file genome from genome_files.collect()
  file quality from quality_files.collect()

  output:
  set file_id, "*" into pred

  script:
"""
ribotish predict -t ${bam[0]} -b ${bam[1]} -g ${gff} -f ${genome} \
-o ${file_id}_pred.txt --harr -p ${task.cpus} --alt
"""
}
