#!/usr/bin/env bash

set -e

usage() { echo "Usage: $0 -b <bamfile.bam> -o <outputName> -s <binSize> -p <CPUs>" 1>&2; exit 1; }

cpus=4
binSize=1

while getopts "hb:o:s:p:" arg; do
  case $arg in
    h)
      usage
      ;;
    b)
      bam=$OPTARG
      ;;
    o)
      output=$OPTARG
      ;;
    s)
      binSize=$OPTARG
      ;;
    p)
      cpus=$OPTARG
      ;;
    \?)
      echo "$OPTARG : invalid option"
      usage
      ;;
     :)
      echo "$OPTARG requiert an argument"
      usage
      ;;

  esac
done

hg38=$(samtools view ${bam} | awk '{print $1}' | sort | uniq | wc -l)
factor=$(echo "1000000/($hg38)" | bc -l)
echo "hg38 counts : $hg38"
echo "scaling factor : $factor"
echo "bamCoverage -p ${cpus} --scaleFactor ${factor} --binSize ${binSize} -b ${bam} -o ${output}"

bamCoverage -p ${cpus} --scaleFactor ${factor} --binSize ${binSize} -b ${bam} -o ${output}
