/*
*	RibosomeProfiling mapping
*/

params.input = ""
params.genome = ""
params.output = ""

log.info "input files (rRNA depleted fastq) : ${params.input}"
log.info "genome : ${params.genome}"
log.info "output folder : ${params.output}"

Channel
  .fromPath( params.genome )
  .ifEmpty { error "Cannot find any fasta files matching: ${params.genome}" }
  .set { fasta_file }

Channel
  .fromPath( params.input )
  .ifEmpty { error "Cannot find any input files for mapping : ${params.input}" }
  .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
  .set { input_file }

/*                      fasta indexing                                     */

process index_fasta {
  tag "$fasta.baseName"
  publishDir "${params.output}/index/", mode: 'copy'

  input:
    file fasta from fasta_file

  output:
    file "*.ht2" into index_files

  script:
"""
hisat2-build -p ${task.cpus} ${fasta} ${fasta.baseName}
"""
}

/*                    mapping for single-end data                          */

process mapping_fastq {
  tag "$file_id"

  input:
  set file_id, file(reads) from input_file
  file index from index_files.collect()

  output:
  set file_id, "*.bam" into bam_files
  file "*_report.txt" into mapping_report

  script:
  index_id = index[0]
  for (index_file in index) {
    if (index_file =~ /.*\.1\.ht2/ && !(index_file =~ /.*\.rev\.1\.ht2/)) {
        index_id = ( index_file =~ /(.*)\.1\.ht2/)[0][1]
    }
  }
"""
hisat2 -p ${task.cpus} \
 -x ${index_id} \
 -U ${reads} 2> \
 --norc \
${file_id}_hisat2_report.txt | \
samtools view -Sb -F 4 - > ${file_id}.bam

if grep -q "Error" ${file_id}_hisat2_report.txt; then
  exit 1
fi

"""
}

/*                   sorting and indexing bams                            */

process sort_bam {
  tag "$file_id"
  publishDir "${params.output}/mapping/", mode: 'copy'

  input:
    set file_id, file(bam) from bam_files

  output:
    set file_id, "*_sorted.{bam,bam.bai}" into sorted_bam_files

  script:
"""
samtools sort -@ ${task.cpus} -O BAM -o ${file_id}_sorted.bam ${bam}
samtools index ${file_id}_sorted.bam
"""
}
