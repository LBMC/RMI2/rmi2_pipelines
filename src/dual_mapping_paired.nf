///////////////////////////////////////////////////////////////////////////////
//////////////////////////////  INDEXING GENOMES  /////////////////////////////
///////////////////////////////////////////////////////////////////////////////

params.fasta = "data/genome/NC001802.1.fa"
log.info "fasta files : ${params.fasta}"
params.output = "results"
log.info "saving file into ${params.output}"

Channel
  .fromPath( params.fasta )
  .ifEmpty { error "Cannot find any fasta files matching: ${params.fasta}" }
  .set { fasta_file }

fasta_file.into{fasta_hisat ; fasta_bowtie}

/* HISAT */
process index_fasta_hisat {
  tag "$fasta.baseName"
  publishDir "data/indexes/${fasta.baseName}_hisat/", mode: 'copy'

  input:
    file fasta from fasta_hisat

  output:
    file "*ht2" into index_files_hisat

  script:
"""
hisat2-build -p ${task.cpus} ${fasta} ${fasta.baseName}
"""
}

/* BOWTIE 2 */

process index_fasta_bowtie {
  tag "$fasta.baseName"
  publishDir "results/${fasta.baseName}_bowtie2/", mode: 'copy'

  input:
    file fasta from fasta_bowtie

  output:
    file "*bt2" into index_files_bowtie
    file "*_report.txt" into indexing_report

  script:
"""
bowtie2-build --threads ${task.cpus} -f ${fasta} ${fasta.baseName} \
&> ${fasta.baseName}_bowtie2_report.txt

if grep -q "Error" ${fasta.baseName}_bowtie_report.txt; then
  exit 1
fi
"""
}

///////////////////////////////////////////////////////////////////////////////
//////////////////////////////  MAPPING GENOMES  /////////////////////////////
///////////////////////////////////////////////////////////////////////////////

params.fastq = "$baseDir/data/fastq/*{R1,R2}.fastq.gz"
// params.index = "$baseDir/data/index/*.index*"

log.info "fastq files : ${params.fastq}"
// log.info "index files : ${params.index}"

Channel
  .fromFilePairs( params.fastq )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.fastq}" }
  .set { fastq_files }
/*Channel
  .fromPath( params.index )
  .ifEmpty { error "Cannot find any index files matching: ${params.index}" }
  .set { index_files }*/

/* HISAIT */

process hisat2 {
  tag "$file_id"

  input:
    set file_id, file(fastq_filtred) from fastq_files
    file index from index_files_hisat.collect()

  output:
    set file_id, "*.bam" into bam_hisat
    file "*.txt" into hisat_report
    set file_id, "*.fastq.gz" into reads_non_aligned

  script:
  index_id = index[0]
  for (index_file in index) {
    if (index_file =~ /.*\.1\.ht2/ && !(index_file =~ /.*\.rev\.1\.ht2/)) {
        index_id = ( index_file =~ /(.*)\.1\.ht2/)[0][1]
    }
  }
"""
hisat2 -x ${index_id} -p ${task.cpus} \
-1 ${fastq_filtred[0]} -2 ${fastq_filtred[1]} --un-conc-gz ${file_id}_notaligned_hisat_R%.fastq.gz \
--rna-strandness 'F' \
2> ${file_id}_hisat2_NY5.txt | samtools view -F 4 -F 16 -Sb - > ${file_id}.bam

if grep -q "ERR" ${file_id}_hisat2_NY5.txt ; then
  exit 1
fi
"""
}

/* BOWTIE */

process bowtie {
  tag "$file_id"

  input:
  set file_id, file(reads) from reads_non_aligned
  file index from index_files_bowtie.collect()

  output:
  set file_id, index_id, "*.bam" into bam_bowtie
  file "*_report.txt" into mapping_report

  script:
index_id = index[0]
for (index_file in index) {
  if (index_file =~ /.*\.1\.bt2/ && !(index_file =~ /.*\.rev\.1\.bt2/)) {
      index_id = ( index_file =~ /(.*)\.1\.bt2/)[0][1]
  }
}
"""
bowtie2 --sensitive --fr -p ${task.cpus} -x ${index_id} \
-1 ${reads[0]} -2 ${reads[1]} 2> \
${file_id}_bowtie2_report_tmp.txt | \
samtools view -F 4 -F 16 -Sb - > ${file_id}_bowtie.bam

if grep -q "Error" ${file_id}_bowtie2_report_tmp.txt; then
  exit 1
fi
tail -n 19 ${file_id}_bowtie2_report_tmp.txt > ${file_id}_bowtie2_report.txt
"""
}

///////////////////////////////////////////////////////////////////////////////
//////////////////////////////  MERGE BAM  /////////////////////////////
///////////////////////////////////////////////////////////////////////////////

bam_bowtie.join(bam_hisat)
          .set{merged_bam}

//merged_bam.println()

process merge_bam{
  publishDir "${params.output}/05_${index_id}_mergedBAM/", mode: 'copy'

  input:
  set file_id, index_id, file(bam_bowtie), file(bam_hisat) from merged_bam
  file(hisat_txt) from  hisat_report
  file(bowtie_txt) from mapping_report

  output:
  file "*merged.{bam,bam.bai}" into final_flow
  file "*.txt" into final_report

  """
  samtools sort -@ ${task.cpus} -o ${file_id}_bowtie_sort.bam ${bam_bowtie} && \
  samtools sort -@ ${task.cpus} -o ${file_id}_hisat_sort.bam ${bam_hisat} && \
  samtools merge ${file_id}_merged.bam *sort.bam && \
  samtools index ${file_id}_merged.bam

  for i in *.txt ; do
    echo \$i
    cat \$i >> \${i::-4}_new.txt
  done

  """

}
