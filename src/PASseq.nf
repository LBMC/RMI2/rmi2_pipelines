/*
*	RibosomeProfiling Analysis pipeline
*/

/* Trimming */
params.output = "results"
params.fastq_raw = "${params.output}/00_demultiplexing/*.fastq.gz"

Channel
  .fromPath( params.fastq_raw )
  .ifEmpty { error "Cannot find any files matching: ${params.fastq_raw}" }
  .map { it -> [(it.baseName =~ /([^\.]*)/)[0][1], it]}
  .set { fastq_raw_flow }


process trimming {
  tag "$file_id"

  input:
  set file_id, file(fastq_raw) from fastq_raw_flow

  output:
  set file_id, "*_cut.fastq.gz" into fastq_trim_filt
  file "*.txt" into log_trim

  script:
  """
  cutadapt -a AAAAAAAAAAAAAAAAAAAAAGATCGGAAGAGCGGTTCAGCAGGAATGCCGAG -m 15\
  -u 2 -o ${file_id}_cut.fastq.gz \
  ${fastq_raw} > ${file_id}_report.txt
  """
}

/* rRNA and tRNA filtering */

params.indexrRNA = "results/human_rRNA_tRNA/*.bt2"
log.info "index files rRNA : ${params.indexrRNA}"

Channel
  .fromPath( params.indexrRNA )
  .ifEmpty { error "Cannot find any index files matching: ${params.indexrRNA}" }
  .set { rRNA_index_files }

process rRNA_removal {
  tag "$file_id"
  publishDir "${params.output}/02_rRNA_depletion/", mode: 'copy'

  input:
  set file_id, file(reads) from fastq_trim_filt
  file index from rRNA_index_files.toList()

  output:
  set file_id, "*.fastq.gz" into rRNA_removed_reads
  file "*.txt" into bowtie_report

  script:
  index_id = index[0]
  for (index_file in index) {
    if (index_file =~ /.*\.1\.bt2/ && !(index_file =~ /.*\.rev\.1\.bt2/)) {
        index_id = ( index_file =~ /(.*)\.1\.bt2/)[0][1]
    }
  }
"""
zcat ${reads} | bowtie2 --sensitive -p ${task.cpus} -x ${index_id} \
-U - --un-gz ${file_id}_mRNA.fastq.gz 2> \
${file_id}_bowtie2_report.txt > /dev/null

if grep -q "Error " ${file_id}_bowtie2_report.txt; then
  exit 1
fi
"""
}

/*	mapping against human genome with hisat2 */

params.index_hg38 = "/media/adminmanu/Stockage/HISAT2_index_hg38_tran/*.ht2"

log.info "index : ${params.index_hg38}"


Channel
  .fromPath ( params.index_hg38 )
  .ifEmpty { error "Cannot find any hg38 index files matching: ${params.index_hg38}" }
  .set { index_file_hg38 }

process hisat2_human {
  tag "$file_id"


  input:
    set file_id, file(fastq_filtred) from rRNA_removed_reads
    file index from index_file_hg38.toList()

  output:
    set file_id, "*.fastq.gz" into reads_non_aligned_hg38
    set file_id, "*.bam" into reads_aligned_hg38
    file "*.txt" into hisat_report

  script:
  index_id = index[0]
  for (index_file in index) {
    if (index_file =~ /.*\.1\.ht2/ && !(index_file =~ /.*\.rev\.1\.ht2/)) {
        index_id = ( index_file =~ /(.*)\.1\.ht2/)[0][1]
    }
  }
"""
hisat2 -x ${index_id} -p ${task.cpus} \
-U ${fastq_filtred} --un-gz ${file_id}_notaligned.fastq.gz \
--end-to-end  --rna-strandness 'F' \
2> ${file_id}_hisat2.txt | samtools view -bS -F 4 -o ${file_id}.bam

"""
}

/*                   sorting                             */

process index_bam {
  tag "$file_id"
  publishDir "${params.output}/03_mapping/", mode: 'copy'

  input:
    set file_id, file(bam) from reads_aligned_hg38
    file report from hisat_report

  output:
    set file_id, "*_sorted.{bam,bam.bai}" into sorted_bam_files
    file "*.log" into hisat_report_bis

  script:
"""
samtools sort -@ ${task.cpus} -O BAM -o ${file_id}_sorted.bam ${bam}
samtools index ${file_id}_sorted.bam
cat ${report} > ${file_id}.log
"""
}

sorted_bam_files.into{for_htseq}

/*                   HTseq                            */

params.gtf = "$baseDir/data/annotation/*.gtf"
log.info "gtf files : ${params.gtf}"

Channel
  .fromPath( params.gtf )
  .ifEmpty { error "Cannot find any gtf file matching: ${params.gtf}" }
  .set { gtf_file }

process counting {
  tag "$file_id"
  publishDir "${params.output}/04_HTseq/", mode: 'copy'

  input:
  set file_id, file(bam) from for_htseq
  file gtf from gtf_file.toList()

  output:
  file "*.count" into count_files

  script:
"""
htseq-count ${bam[0]} ${gtf} \
            --mode=union \
            -a 10 \
            -s yes \
            -t exon \
            -i gene_id \
            -f bam \
> ${file_id}_exon.count

"""
}
