#! /bin/bash

set -e

# For RNASeq
nextflow src/RNAseq.nf -c src/RNAseq.config \
                       -profile docker\
                       -resume\
                       --do_fastqc true\
                       --do_dedup false\
                       --do_postgenome true\
                       --adaptorR1 "AGATCGGAAGAGCACACGTCTGAACTCCAGTCA"\
                       --adaptorR2 "AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT"\
                       --strand "FR"\
                       --fastq_raw "data/fastq/*{_R1,_R2}_short.fastq.gz"\
                       --output "results"\
                       --filter "data/filter/*.bt2"\
                       --index_genome "data/genome/*.ht2"\
                       --gtf "data/annotation/gencode.v28.annotation_v3.gtf"\
                       --gtf_collapse "data/annotation/gencode.v28.annotation_v3_collapse.gtf"\
                       --index_postgenome "data/post_genome/*.ht2"

# For Ribosome Profiling
nextflow src/RibosomeProfiling.nf -c src/RNAseq.config \
                       -profile docker\
                       -resume\
                       --do_fastqc true\
                       --do_dedup false\
                       --do_postgenome true\
                       --adaptorR1 "AGATCGGAAGAGCACACGTCTGAACTCCAGTCA"\
                       --strand "F"\
                       --fastq_raw "data/fastq/*.fastq.gz"\
                       --output "results"\
                       --filter "data/filter/*.bt2"\
                       --index_genome "data/genome/*.ht2"\
                       --gtf "data/annotation/gencode.v28.annotation_v3.gtf"\
                       --gtf_collapse "data/annotation/gencode.v28.annotation_v3_collapse.gtf"\
                       --index_postgenome "data/post_genome/*.ht2"
