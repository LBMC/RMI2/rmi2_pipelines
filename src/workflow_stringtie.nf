nextflow.enable.dsl=2

/*
./nextflow src/solution_RNASeq.nf --fastq "data/tiny_dataset/fastq/tiny2_R{1,2}.fastq.gz" --fasta "data/tiny_dataset/fasta/tiny_v2_10.fasta" --bed "data/tiny_dataset/annot/tiny.bed" -profile docker
*/

params.fastq_raw = "data/fastq/*.fastq.gz"
params.output = "results"
params.index_genome = "data/genome/*.ht2"
params.gtf = "data/annotation/*.gtf"

log.info "input raw : ${params.fastq_raw}"
log.info "outut directory : ${params.output}"
log.info "genome index : ${params.index_genome}"
log.info "gtf file : ${params.gtf}"

Channel
   .fromPath ( params.index_genome )
   .ifEmpty { error "Cannot find any index files matching: ${params.index_genome}" }
   .set { GENOME_INDEX }
Channel
   .fromPath ( params.gtf )
   .ifEmpty { error "Cannot find any files matching: ${params.gtf}" }
   .set { GTF }
Channel
  .fromFilePairs( params.fastq_raw )
  .ifEmpty { error "Cannot find any fastq files matching: ${params.fastq}" }
  .set { FASTQ_FILES }


include { paired_end } from './nf_modules/hisat2/rmi2_pipeline'
include { abundance } from './nf_modules/stringtie/rmi2_pipeline'

workflow {
    paired_end(FASTQ_FILES, GENOME_INDEX.collect(), params.output)
    abundance(paired_end.out.BAM, GTF.collect(), params.output)
}
