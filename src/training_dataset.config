profiles {
  docker {
    docker.temp = "auto"
    docker.enabled = true
    process {
      withName: build_synthetic_bed {
        container = "lbmc/bedtools:2.25.0"
        cpus = 1
      }
      withName: fasta_from_bed {
        container = "lbmc/bedtools:2.25.0"
        cpus = 1
      }
      withName: index_fasta {
        container = "lbmc/bowtie2:2.3.4.1"
        cpus = 4
      }
      withName: mapping_fastq_paired {
        container = "lbmc/bowtie2:2.3.4.1"
        cpus = 4
      }
      withName: bam_2_fastq_paired {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: filter_bam_paired {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: sort_bam_paired {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: index_bam_paired {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: mapping_fastq_single {
        container = "lbmc/bowtie2:2.3.4.1"
        cpus = 4
      }
      withName: bam_2_fastq_single {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: filter_bam_single {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: sort_bam_single {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: index_bam_single {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
    }
  }
  singularity {
    singularity.enabled = true
    singularity.cacheDir = "./bin/"
    process {
      withName: build_synthetic_bed {
        container = "lbmc/bedtools:2.25.0"
        cpus = 1
      }
      withName: fasta_from_bed {
        container = "lbmc/bedtools:2.25.0"
        cpus = 1
      }
      withName: index_fasta {
        container = "lbmc/bowtie2:2.3.4.1"
        cpus = 4
      }
      withName: mapping_fastq_single {
        container = "lbmc/bowtie2:2.3.4.1"
        cpus = 4
      }
      withName: mapping_fastq_paired {
        container = "lbmc/bowtie2:2.3.4.1"
        cpus = 4
      }
      withName: bam_2_fastq_paired {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: filter_bam_paired {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: sort_bam_paired {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: index_bam_paired {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: bam_2_fastq_single {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: filter_bam_single {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: sort_bam_single {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
      withName: index_bam_single {
        container = "lbmc/samtools:1.7"
        cpus = 4
      }
    }
  }
  psmn {
    process{
      withName: build_synthetic_bed {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/bedtools_2.25.0"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 1
        memory = "20GB"
        time = "12h"
        queue = "monointeldeb128"
      }
      withName: fasta_from_bed {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/bedtools_2.25.0"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 1
        memory = "20GB"
        time = "12h"
        queue = "monointeldeb128"
      }
      withName: index_fasta {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/bowtie2_2.3.4.1"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 32
        memory = "20GB"
        time = "12h"
        queue = "CLG6242deb384A,CLG6242deb384C,CLG5218deb192A,CLG5218deb192B,CLG5218deb192C,CLG5218deb192D,SLG5118deb96,SLG6142deb384A,SLG6142deb384B,SLG6142deb384C,SLG6142deb384D"
        penv = "openmp32"
      }
      withName: mapping_fastq_paired {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/bowtie2_2.3.4.1"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 32
        memory = "30GB"
        time = "24h"
        queue = "CLG6242deb384A,CLG6242deb384C,CLG5218deb192A,CLG5218deb192B,CLG5218deb192C,CLG5218deb192D,SLG5118deb96,SLG6142deb384A,SLG6142deb384B,SLG6142deb384C,SLG6142deb384D"
        penv = "openmp32"
      }
      withName: bam_2_fastq_paired {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/samtools_1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 32
        memory = "30GB"
        time = "24h"
        queue = "CLG6242deb384A,CLG6242deb384C,CLG5218deb192A,CLG5218deb192B,CLG5218deb192C,CLG5218deb192D,SLG5118deb96,SLG6142deb384A,SLG6142deb384B,SLG6142deb384C,SLG6142deb384D"
        penv = "openmp32"
      }
      withName: sort_bam_paired {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/samtools_1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 32
        memory = "30GB"
        time = "24h"
        queue = "CLG6242deb384A,CLG6242deb384C,CLG5218deb192A,CLG5218deb192B,CLG5218deb192C,CLG5218deb192D,SLG5118deb96,SLG6142deb384A,SLG6142deb384B,SLG6142deb384C,SLG6142deb384D"
        penv = "openmp32"
      }
      withName: index_bam_paired {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/samtools_1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 32
        memory = "30GB"
        time = "24h"
        queue = "CLG6242deb384A,CLG6242deb384C,CLG5218deb192A,CLG5218deb192B,CLG5218deb192C,CLG5218deb192D,SLG5118deb96,SLG6142deb384A,SLG6142deb384B,SLG6142deb384C,SLG6142deb384D"
        penv = "openmp32"
      }
      withName: mapping_fastq_single {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/bowtie2_2.3.4.1"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 32
        memory = "30GB"
        time = "24h"
        queue = "CLG6242deb384A,CLG6242deb384C,CLG5218deb192A,CLG5218deb192B,CLG5218deb192C,CLG5218deb192D,SLG5118deb96,SLG6142deb384A,SLG6142deb384B,SLG6142deb384C,SLG6142deb384D"
        penv = "openmp32"
      }
      withName: bam_2_fastq_single {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/samtools_1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 32
        memory = "30GB"
        time = "24h"
        queue = "CLG6242deb384A,CLG6242deb384C,CLG5218deb192A,CLG5218deb192B,CLG5218deb192C,CLG5218deb192D,SLG5118deb96,SLG6142deb384A,SLG6142deb384B,SLG6142deb384C,SLG6142deb384D"
        penv = "openmp32"
      }
      withName: sort_bam_single {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/samtools_1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 32
        memory = "30GB"
        time = "24h"
        queue = "CLG6242deb384A,CLG6242deb384C,CLG5218deb192A,CLG5218deb192B,CLG5218deb192C,CLG5218deb192D,SLG5118deb96,SLG6142deb384A,SLG6142deb384B,SLG6142deb384C,SLG6142deb384D"
        penv = "openmp32"
      }
      withName: index_bam_single {
        beforeScript = "source $baseDir/.conda_psmn.sh"
        conda = "$baseDir/.conda_envs/samtools_1.7"
        executor = "sge"
        clusterOptions = "-m e -cwd -V"
        cpus = 32
        memory = "30GB"
        time = "24h"
        queue = "CLG6242deb384A,CLG6242deb384C,CLG5218deb192A,CLG5218deb192B,CLG5218deb192C,CLG5218deb192D,SLG5118deb96,SLG6142deb384A,SLG6142deb384B,SLG6142deb384C,SLG6142deb384D"
        penv = "openmp32"
      }
    }
  }
  ccin2p3 {
    singularity.enabled = true
    singularity.cacheDir = "$baseDir/.singularity_in2p3/"
    singularity.runOptions = "--bind /pbs,/sps,/scratch"
    process{
      withName: fasta_from_bed {
        container = "lbmc/bedtools:2.25.0"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
    }
    process{
      withName: build_synthetic_bed {
        container = "lbmc/bedtools:2.25.0"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
      withName: fasta_from_bed {
        container = "lbmc/bedtools:2.25.0"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
      withName: index_fasta {
        container = "lbmc/bowtie2:2.3.4.1"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
      withName: mapping_fastq_paired {
        container = "lbmc/bowtie2:2.3.4.1"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
      withName: bam_2_fastq_paired {
        container = "lbmc/samtools:1.7"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
      withName: sort_bam_paired {
        container = "lbmc/samtools:1.7"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
      withName: index_bam_paired {
        container = "lbmc/samtools:1.7"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
      withName: mapping_fastq_single {
        container = "lbmc/bowtie2:2.3.4.1"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
      withName: bam_2_fastq_single {
        container = "lbmc/samtools:1.7"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
      withName: sort_bam_single {
        container = "lbmc/samtools:1.7"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
      withName: index_bam_single {
        container = "lbmc/samtools:1.7"
        scratch = true
        stageInMode = "copy"
        stageOutMode = "rsync"
        executor = "sge"
        clusterOptions = "-P P_lbmc -l os=cl7 -l sps=1 -r n"
        cpus = 1
        queue = "huge"
      }
    }
  }
}
