nextflow.enable.dsl=2

/*
nextflow src/RMI2_ribodiff.nf --exp_outline "EXPTOUTLINE.csv" --cnt_table "SupT1_36h.count" --output "results/ribodiff" -profile docker
*/

log.info "exp_outline file : ${params.exp_outline}"
log.info "cnt_table file : ${params.cnt_table}"
log.info "output folder : ${params.output}"

Channel
  .fromPath( params.exp_outline )
  .ifEmpty { error "Cannot find any file matching: ${params.exp_outline}" }
  .set { exp_outline_file }
Channel
  .fromPath( params.cnt_table )
  .ifEmpty { error "Cannot find any file matching: ${params.cnt_table}" }
  .set { cnt_table_file }

include { ribodiff } from './nf_modules/ribodiff/rmi2_pipeline'

workflow {
    ribodiff(exp_outline_file, cnt_table_file, params.output)
    }
